import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import PropTypes from 'prop-types'

import { Link } from 'react-router-dom'


//MUI IMPORTS
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

import appIcon from '../images/monkey.png'

// REDUX IMPORTS
import { connect } from 'react-redux'
import { signupUser } from '../redux/Actions/userActions'


const styles = (theme) => ({
    ...theme.spreadThis
});

class signup extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            handle: '',
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.UI.error) {
            this.setState({ errors: nextProps.UI.errors });
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            loading: true,
        });
        const newUser = {
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            handle: this.state.handle,

        };
        this.props.signupUser(newUser, this.props.history);
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        const { classes, UI: { loading } } = this.props;
        const { errors } = this.state;
        return (
            <Grid container className={classes.form}>
                <Grid item sm>

                </Grid>
                <Grid item sm>
                    <img src={appIcon} alt="monkey" className={classes.image} />
                    <Typography variant="h2" className={classes.pageTitle}>
                        Signup
                    </Typography>
                    <form noValidate onSubmit={this.handleSubmit}>
                        <TextField id="email" name="email" type="email" label="Email" helperText={errors.email} error={errors.email ? true : false}
                            className={classes.textField} value={this.state.email} onChange={this.handleChange} fullWidth />
                        <TextField id="password" name="password" type="password" label="Password" helperText={errors.password} error={errors.password ? true : false}
                            className={classes.textField} value={this.state.password} onChange={this.handleChange} fullWidth />
                        <TextField id="confirmPassword" name="confirmPassword" type="password" label="Confirm Password" helperText={errors.confirmPassword} error={errors.confirmPassword ? true : false}
                            className={classes.textField} value={this.state.confirmPassword} onChange={this.handleChange} fullWidth />
                        <TextField id="handle" name="handle" type="handle" label="Handle" helperText={errors.handle} error={errors.handle ? true : false}
                            className={classes.textField} value={this.state.handle} onChange={this.handleChange} fullWidth />
                        {errors.general && (
                            <Typography variant="body2" className={classes.customError}>
                                {errors.general}
                            </Typography>
                        )}
                        <Button type="submit" variant="contained" color="primary" className={classes.button} disabled={loading}>
                            Signup
                            {loading && (<CircularProgress className={classes.loadingProgress} />)}
                        </Button>
                        <br />
                        <small>you already have an account ? login up <Link to="/login">here</Link></small>
                    </form>
                </Grid>
                <Grid item sm>

                </Grid>
            </Grid>
        )
    }
}

signup.propTypes = {
    classes: PropTypes.object.isRequired,
    signupUser: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    user: state.user,
    UI: state.UI
})

export default connect(mapStateToProps, { signupUser })(withStyles(styles)(signup))
