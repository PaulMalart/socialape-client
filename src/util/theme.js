export default {
    palette: {
        primary: {
            light: '#33c9dc',
            main: '#00bcd4',
            dark: '#008394',
            contrastText: '#fff'
        },
        secondary: {
            light: '#ff6333',
            main: '#ff3d00',
            dark: '#b22a00',
            contrastText: '#fff'
        },
    },
    typography: {
        useNextVariants: true
    },
    spreadThis: {
        form: {
            textAlign: 'center'
        },
        image: {
            margin: "20px auto 20px auto"
        },
        pageTitle: {
            margin: "10px auto 10px auto"
        },
        textField: {
            margin: "10px auto 10px auto"
        },
        button: {
            marginTop: "20px",
            position: 'relative'
        },
        customError: {
            color: 'red',
            fontSize: '.8rem',
            marginTop: 10
        },
        loadingProgress: {
            position: 'absolute',
            size: '20'
        }
    }
}