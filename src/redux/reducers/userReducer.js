import { SET_USER, SET_UNAUTHENTICATED, SET_AUTHENTICATED, LOADING_USER } from '../types'

const initialState = {
    authenticated: false,
    credentials: {},
    likes: [],
    notifications: [],
};

export default function(state = initialState, action){
    switch(action){
        case SET_AUTHENTICATED:
            return {
                ...state,
                authenticated: true
            }
        case SET_UNAUTHENTICATED:
            return initialState;
        case SET_USER:
            return {
                authenticated: true,
                loading: false,
                ...action.payload
            }
        case LOADING_USER:
            return {
                ...state,
                authenticated: true
            }
        default:
            return state;
    }
}