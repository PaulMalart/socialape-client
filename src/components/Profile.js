import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import withStyles from '@material-ui/core/styles/withStyles'

import { connect } from 'react-redux'

import dayjs from 'dayjs';

import { Link } from 'react-router-dom'

//mui imports
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import MuiLink from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'
import LocationOn from '@material-ui/icons/LocationOn'
import LinkIcon from '@material-ui/icons/Link'
import CalendarToday from '@material-ui/icons/CalendarToday'


const styles = (theme) => ({
    paper: {
        padding: 20
    },
    profile: {
        '& .image-wrapper': {
            textAlign: 'center',
            position: 'relative',
            '& button': {
                position: 'absolute',
                top: '80%',
                left: '70%'
            }
        },
        '& .profile-image': {
            width: 200,
            height: 200,
            objectFit: 'cover',
            maxWidth: '100%',
            borderRadius: '50%'
        },
        '& .profile-details': {
            textAlign: 'center',
            '& span, svg': {
                verticalAlign: 'middle'
            },
            '& a': {
                color: theme.palette.primary.main
            }
        },
        '& hr': {
            border: 'none',
            margin: '0 0 10px 0'
        },
        '& svg.button': {
            '&:hover': {
                cursor: 'pointer'
            }
        }
    },
    buttons: {
        textAlign: 'center',
        '& a': {
            margin: '20px 10px'
        }
    }
});

class Profile extends Component {
    render() {
        const {
            classes,
            user: {
                credentials: { handle, createdAt, imageUrl, website, location, bio },
                loading, authenticated
            }
        } = this.props;

        let profileMarkup = !loading ? (authenticated ? (
                <Paper className={classes.paper}>
                    <Typography variant="body2" align="center">
                        No Profile Found, Please login
                    </Typography>
                    <div className={classes.buttons} >
                        <Button variant="contained" color="primary" component={Link} to='/login'>Login</Button>
                        <Button variant="contained" color="secondary" component={Link} to='/signup'>Signup</Button>
                    </div>
                </Paper>
        ) : (
            <Paper className={classes.paper} >
                <div className='image-wrapper'>
                    <img src={imageUrl} alt='profile' className='profile-image' />
                </div>
                <hr />
                <div className="profile-details">
                    <MuiLink component={Link} to={`/users/${handle}`} color="primary" variant="h5">
                        @{handle}
                    </MuiLink>
                    <hr />
                    {bio && <Typography variant="body2">{bio}</Typography>}
                    <hr />
                    {location && (
                        <Fragment>
                            <LocationOn color="primary" /> <span>{location}</span>
                            <hr />
                        </Fragment>
                    )}
                    {website && (
                        <Fragment>
                            <LinkIcon color="primary" /> <a href={website} target="_blank" rem="noopener noreferrer">
                                {'  '}{website}
                            </a>
                            <hr />
                        </Fragment>
                    )}
                    <CalendarToday color='primary' />{'  '}
                    <span>
                        Joined {dayjs(createdAt).format('MMM YYYY')}
                    </span>
                </div>
            </Paper>
            )) : (<p>loading...</p>)

        return profileMarkup;
    }
}

Profile.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
    user: state.user,
})

export default connect(mapStateToProps)(withStyles(styles)(Profile))
